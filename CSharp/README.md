Use StyleCop for ordering
https://github.com/DotNetAnalyzers/StyleCopAnalyzers/blob/master/documentation/SA1201.md

### Namespaces

Namespaces are all **PascalCase**, multiple words concatenated together, without hyphens ( - ) or underscores ( \_ ). The exception to this rule are acronyms like GUI or HUD, which can be uppercase:

```cs
AAE.AppName.Infrastructure
```

### Classes & Interfaces

Classes and interfaces are written in **PascalCase**. For example `RadialSlider`. 

### Methods

Methods are written in **PascalCase**. For example `DoSomething()`. 

### Fields

All non-static fields are written **camelCase**. Do not use underscores. Exception: prefix private class variables with an underscore (consistent with .Net Framework)

```csharp
// Correct
public int publicField;
int packagePrivate;
private int myPrivate;
protected int myProtected;

// Avoid
public DateTime client_Appointment;
public TimeSpan time_Left;

// Exception
private DataClass _myPrivateVariable
```

### Properties

All properties are written in **PascalCase**.  Initialized as declaration unless logic required, and then declared in the constructor. For example:

```csharp
public class Document {
    private int _pageNumber;

    public Document(int pageNumber) {
        _pageNumber = pageNumber;
        if (pageNumber > 5)
            Color = "Black";
        else
            Color = "White";
    }

    public string Description { get; set;} = string.Empty;
    public string Color { get; set; }

    public int PageNumber 
    {
        get { return _pageNumber; }
        set { _pageNumber = value; }
    }
}

```

### Parameters

Parameters are written in **camelCase**.

```csharp
// Correct
void DoSomething(Vector3 location)

// Avoid
void DoSomething(Vector3 Location)
```

### Initial Values

Initial default values for property types should be `default` EXCEPT `string`, which should be `string.Empty`

```csharp
// Correct
public string SomeProperty { get; set; } = string.Empty;
public int ThisNumber { get; set; } = default;
public decimal? DecimalFigure { get; set; } = default;
public object SomeObject { get; set; } = default;

// Avoid
public string SomeProperty { get; set; } = "";
public int ThisNumber { get; set; } = -1;
public decimal? DecimalFigure { get; set; } = null;
public object SomeObject { get; set; } = null;
```

Single character values are to be avoided except for temporary looping variables.

### Actions

Actions are written in **PascalCase**. For example:

```csharp
public event Action<int> ValueChanged;
```

### Misc

In code, acronyms should be treated as words. For example:

```csharp
// Correct
XmlHttpRequest
String url
findPostById

// Avoid
XMLHTTPRequest
String URL
findPostByID
```  

## Declarations

### Access Level Modifiers

Access level modifiers should be explicitly defined for classes, methods and member variables.

### Fields & Variables

Prefer single declaration per line.

```csharp
// Correct
string username;
string twitterHandle;

// Avoid
string username, twitterHandle;
```

Use predefined type names instead of system type names like Int16, Single, String, etc
```csharp
// Correct
string firstName;
int lastIndex;
bool isSaved;
    
// Avoid
String firstName;
Int32 lastIndex;
Boolean isSaved;
```

### Classes

Exactly one class per source file, although inner classes are encouraged where scoping appropriate.

### Interfaces

All interfaces should be prefaced with the letter **I**. 

```csharp
// Correct
IRadialSlider

// Avoid
RadialSlider
```

### Enums

Use singular names for enums. Exception: bit field enums. Consistent with .NET Framework. Plural flags because enum can hold multiple values (using bitwise 'OR').

```csharp
// Correct
public enum Color
{
    Red,
    Green,
    Blue,
    Yellow,
    Magenta,
    Cyan
}
    
// Exception
[Flags]
public enum Dockings
{
    None = 0,
    Top = 1, 
    Right = 2, 
    Bottom = 4,
    Left = 8
}
```

Do not explicitly specify a type of en enum or values of enums (except bit fields or enums that are matched to database fields)

```csharp
// Correct
public enum Direction
{
    North,
    East,
    South,
    West
}

// Avoid
public enum Direction : long
{
    North = 1,
    East = 2,
    South = 3,
    West = 4
}

```
