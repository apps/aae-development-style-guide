# NEED TO CLEANUP
## API endpoints and Pinia Stores
```javascript
// -------------------------------------------
// symbols.ts
export const employeeEndpointKey: InjectionKey<string> = Symbol('employeeEndpoint'); 

// -------------------------------------------
// stores/employee.ts
import { reactive, ref, computed } from 'vue';
import { defineStore } from 'pinia';
import type { Employee } from '@/types';
import http from '@/util/http';
import { joinPaths } from '@/util/urlUtility'
export const useEmployeeStore = defineStore('employee', () => {

    const employee = ref({} as Employee)

    let endpoint = '' as string

    // called as early as needed - top level await
    async function initialize(employeeEndpoint: string) {
        endpoint = employeeEndpoint;
    }

    // fetch all employees from API
    async function fetchEmployee(employeeId: string) {
        const { data }: { data: Employee } = await http.get(joinPaths(endpoint, employeeId));
        if (data != undefined) employee.value = data;
    }

    return { employee, initialize, fetchEmployee }
})



// -------------------------------------------
// App.vue
import { inject, onMounted } from 'vue'

const employeeEndpoint = inject(employeeEndpointKey)

onMounted(async () => {
    if (employeeEndpoint == undefined) throw new Error("Employee endpoint is not defined")
    if (employeeId == undefined) throw new Error("EmployeeId is not defined")

    await employeeStore.initialize(employeeEndpoint);
    await employeeStore.fetchEmployee(employeeId);

});
```