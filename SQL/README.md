# SQL Style Guide for AAE

## Table naming conventions
**Singular** and **Lowercase** *table* names:  `person`, `group`, `address`

**snake_case** for *column* names: `id`, `first_name`, `last_name`

**Linking Tables** done in snake_case: `person_groupmember`, 

## Key Fields

### All Fields
**Avoid** using reserved keywords (`ADD, END, COLUMN`) for field names.  Field names that are not reserved but are keywords (`description, profile`), use []'s.
```sql
SELECT * FROM [profile]
```


### Primary Keys
**Single column primary key fields should be named `id`**.  Always have an `int` or `bigint` primary (preferably with an `IDENTITY`), even if there is a "true" primary key that isn't an `int` in the data
```sql
CREATE TABLE person (
  id            bigint PRIMARY KEY,
  full_name     nvarchar(50) NOT NULL,
  birth_date    date NOT NULL);
```

### Foreign Keys
**Foreign key fields should be a combination of the name of the referenced table and the name of the referenced field**
```sql
CREATE TABLE team_member (
  team_id       bigint NOT NULL REFERENCES team(id),
  person_id     bigint NOT NULL REFERENCES person(id),
  CONSTRAINT team_member_pkey PRIMARY KEY (team_id, person_id));
```

### Indexes
Indexes should be explicitly named and include both the table name and the column name(s) indexed.
```sql
CREATE TABLE person (
  id          int PRIMARY KEY,
  email       nvarchar(50) NOT NULL,
  first_name  nvarchar(50) NOT NULL,
  last_name   nvarchar(50) NOT NULL,

CREATE INDEX person_ix_first_name_last_name ON person (first_name, last_name);
```

## Column prefixes and suffixes

"is_" or "has_" for boolean-type
"_date" for date-type
"_time" for datetime-type



## SQL Queries

Use qualifiers for all column names (`person.`, `[profile].`)

```sql
person.first_name
[profile].web_link
```
### Parameters
No standards for case in parameters.  Best practice will tend to be **camelCase for single parameters** and **snake_case for objects**.  This is due to C# style guide which has method parameters as camelCase and persistence DTOs matching SQL table field names (which are snake_case).   Following this standard makes anonymous object creation much easier for use with Dapper.
Also, use `{nameof(myParameter)}` for single paramaeters, to make renaming of parameters flow into the SQL strings as well.
```csharp
public PersonAttribute Get(int personID) {
  using (var conn = new SqlConnection('some_connection_string'))
  {
    conn.Open();
    string sql = $"SELECT * FROM person_attributes WHERE person_id=@{nameof(personID)};";
    return conn.QueryFirstOrDefault<PersonAttribute>(sql, new { personID });
  }
}
```
```csharp
public class PersonPersistDTO
{
  public int id { get; set;}
  public string name { get; set;}
  public DateTime birth_date { get; set;}
}

public void Save(PersonPersistDTO dto) 
{
  using (var conn = new SqlConnection('some_connection_string'))
  {
    conn.Open();
    string sql = "UPDATE person SET name=@name, birth_date=@birth_date WHERE id=@id;";
    return conn.Execute(sql, dto);
  }
}
```